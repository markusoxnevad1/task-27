﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PGMOnlineTask27.Models;

namespace PGMOnlineTask27.controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupervisorController : ControllerBase
    {
        private readonly SupervisorContext _context;

        public SupervisorController(SupervisorContext context)
        {
            _context = context;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Supervisor>> GetASupervisor()
        {
            return _context.Supervisor;
        }
        //  Creates a new object
        [HttpPost]
        public ActionResult<Supervisor> CreateSupervisor(Supervisor supervisor)
        {
            _context.Supervisor.Add(supervisor);
            _context.SaveChanges();

            return CreatedAtAction("GetSpecificSupervisorByID", new Supervisor { Id = supervisor.Id }, supervisor);
        }
        //  Retrieves specific object by Id
        [HttpGet("{id}")]
        public ActionResult<Supervisor> GetSpecificSupervisorByID(int id)
        {
            var supervisor = _context.Supervisor.Find(id);
            return supervisor;
        }
        //  Udates object by Id
        [HttpPut("{Id}")]
        public ActionResult UpdateASupervisor(int id, Supervisor supervisor)
        {
            if(id != supervisor.Id)
            {
                return BadRequest();
            }
            _context.Entry(supervisor).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }
        //  Deletes object by Id
        [HttpDelete("{Id}")]
        public ActionResult<Supervisor> DeletedASupervisor(int id)
        {
            var supervisor = _context.Supervisor.Find(id);

            if(supervisor == null)
            {
                return NotFound();
            }

            _context.Supervisor.Remove(supervisor);
            _context.SaveChanges();

            return supervisor;
        }
    }
}